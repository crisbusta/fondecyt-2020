from django.db import models
from django.contrib.auth.models import User

class Universidad(models.Model):
    name = models.CharField(max_length=80)
    desc = models.TextField(max_length=200, null=True)
    web = models.URLField(max_length=200, null=True)
    photo = models.ImageField(upload_to='instituciones/', blank=True, null=True)

    def __str__(self):
        return self.name

class Persona(models.Model):
    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=30)
    universidad = models.ForeignKey(Universidad, on_delete=models.CASCADE, default=1)
    resumen = models.TextField(max_length=3000)
    info = models.TextField(max_length=170, default=0)
    photo = models.ImageField(upload_to='equipo/', blank=True, null=True)
    cargos = (
		('In', 'Integrante'),
		('Co', 'Colaborador')
		)
    cargo = models.CharField(max_length=10, choices=cargos, default=0)
    #username = models.OneToOneField(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name

class Noticia(models.Model):
    autor = models.ForeignKey(Persona, on_delete=models.CASCADE, default=1)
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=3000)
    cabecera = models.TextField(max_length=250)
    contenido = models.TextField()
    photo = models.ImageField(upload_to='noticias/', blank=True, null=True)

    def __str__(self):
        return self.titulo


class Document(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField(max_length=255, blank=True)
    autor = models.ForeignKey(Persona, on_delete=models.CASCADE, default=1)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

