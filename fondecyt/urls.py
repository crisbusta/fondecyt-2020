from django.urls import path
from fondecyt import views
app_name = 'fondecyt'

urlpatterns = [
    path('', views.home, name='home'),
    path('proyecto/', views.proyectos, name="proyecto"),
    path('instituciones/', views.instituciones, name="instituciones"),
    path('equipo/', views.equipo, name="equipo"),
    path('equipo/<int:id>', views.detallesEquipo, name="integrante"),
    path('colaboradores/', views.colaboradores, name="colaboradores"),
    path('colabodores/<int:id>', views.detallesColaborador, name="colaborador"),
    path('noticias/', views.noticias, name="noticias"),
    path('noticia/<int:id>', views.detalleNoticia, name="noticia"),
    path('productos/', views.productos, name="productos"),
]