from django.shortcuts import render
from fondecyt.models import (
    Noticia,
    Universidad,
    Persona,
    Document
    )

def home(request):
    data = {}
    template_name = 'home.html'
    data['noticias'] = Noticia.objects.order_by('-id')[:2]

    return render(request, template_name, data)

def proyectos(request):
    data = {}
    template_name = 'proyecto.html'

    return render(request, template_name, data)

def instituciones(request):
    data = {}
    template_name = 'instituciones.html'
    data['instituciones'] = Universidad.objects.all().exclude(id=6)


    return render(request, template_name, data)

def equipo(request):
    data = {}
    template_name = 'equipo.html'
    data['equipo'] = Persona.objects.filter(cargo='In')
    return render(request, template_name, data)

def colaboradores(request):
    data = {}
    template_name = 'colaboradores.html'
    data['colaboradores'] = Persona.objects.filter(cargo='Co')


    return render(request, template_name, data)

def detallesEquipo(request, id):
    data = {}
    template_name = 'integrante.html'
    data['persona'] = Persona.objects.get(pk=id)
    if data['persona'].photo:
        data['foto'] = data['persona'].photo.url
    else:
        pass

    return render(request, template_name, data)

def detallesColaborador(request, id):
    data = {}
    template_name = 'integrante.html'
    data['persona'] = Persona.objects.get(pk=id)
    if data['persona'].photo:
        data['foto'] = data['persona'].photo.url
    else:
        pass

    return render(request, template_name, data)


def productos(request):
    data = {}
    template_name = 'productos.html'

    data['productos'] = Document.objects.all()
    print(data['productos'][0].document.url)

    return render(request, template_name, data)

def noticias(request):
    data = {}
    template_name = 'noticias.html'
    data['noticias'] = Noticia.objects.all()
    cant = Noticia.objects.count()
    for i in range(0,cant):
        if(data['noticias'][i].photo):
            data['noticias'][i].photo = data['noticias'][i].photo
        else:
            data['noticias'][i].photo = 'fondecyt/images/news.jpg'

    return render(request, template_name, data)

def detalleNoticia(request, id):
    data = {}
    template_name = 'noticia.html'
    data['noticia'] = Noticia.objects.get(pk=id)
    if data['noticia'].photo:
        data['foto'] = data['noticia'].photo.url
        contenido = (data['noticia'].contenido).split("\n")
        data['parrafos'] = contenido
    else:
        contenido = (data['noticia'].contenido).split("\n")
        data['parrafos'] = contenido
 

    return render(request, template_name, data)
