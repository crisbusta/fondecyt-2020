from django.contrib import admin
from fondecyt.models import (
    Noticia,
    Universidad,
    Persona,
    Document
    )
# Register your models here.
@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'descripcion', 'autor', 'photo']

@admin.register(Universidad)
class UniversidadAdmin(admin.ModelAdmin):
    list_display = ['name',]

@admin.register(Persona)
class PersonaAdmin(admin.ModelAdmin):
    list_display = ['name', 'lastname', 'universidad']

@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'uploaded_at', 'document']