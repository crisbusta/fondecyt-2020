# Generated by Django 3.0.6 on 2020-10-30 19:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fondecyt', '0017_auto_20201030_1911'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='name',
            field=models.CharField(max_length=150),
        ),
    ]
